import { useState, useEffect } from 'react';
import './App.css';
import axios from 'axios';

interface UserType {
  email: string;
  name: { title: string; first: string; last: string };
  picture: { medium: string };
}

function App() {
  const savedUser: any = localStorage.getItem('user');
  const [user, setUser] = useState<UserType>(JSON.parse(savedUser) || []);

  // Fetch User
  const url = 'https://randomuser.me/api';
  const fetchUsers = async () => {
    try {
      await axios.get(url).then((response) => {
        if (response) {
          setUser(response?.data?.results[0]);
          localStorage.setItem(
            'user',
            JSON.stringify(response?.data?.results[0])
          );
        }
      });
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    if (!savedUser) fetchUsers();
  }, [savedUser]);

  const { email, name, picture } = user || {};
  const { title, first, last } = name || {};
  const { medium } = picture || {};

  return (
    <div className="App">
      <header className="App-header">
        <img src={medium} alt="user" />
        <p>{`${title}${
          title === 'Mr' || title === 'Ms' || title === 'Mrs' ? '.' : ''
        } ${first} ${last}`}</p>
        <p className="email-link">{email}</p>
        <button onClick={() => fetchUsers()} className="button">
          Refresh
        </button>
      </header>
    </div>
  );
}

export default App;
